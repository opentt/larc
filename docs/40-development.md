# Development

!!! info ""
	So, you want to change things...

At the moment this is a container for thoughts and practices.

## New LARC

### Excel and csv

If a new LARC is provided by the [ITTF](https://equipments.ittf.com/#/equipments/racket_coverings) download PDF and export the Excel file.

The Excel file does not contain all data of the online list or PDF, but until things get better, this is what we have to work with.

I have my own naming scheme: `LARC_<number>_<version of year>_<year>.xyz`.
Examples:

- `LARC_44_B_2020.pdf`
- `LARC_45_A_2021.pdf`
- `LARC_46_B_2021.pdf`
- `LARC_47_C_2021.pdf`

See all LARCs so far: [LARC-Archive](https://www.tt-schiri.de/einsatzinformationen/belaglisten/)

Download locations:

- `data/ittf/pdf`
- `data/ittf/xslx`

Fill the Excel file with the missing information.
It misses the "expires on" information of the online list.
Create a new column "custom-expires-on" and fill in the data manually.

Now export the Excel as csv into:

- `data/ittf/csv/larc_<number>.csv`
- separator: colon = ","
- strings enclosed in ""
- utf-8

### JSON files

Add the LARC data into

- `data/basics/larcs.json`

### Generate coverings file

Change LARC number in

- `data/1_coverings_csv2json.sh`

Run the script:

~~~ shell
$> cd data
$> ./1_coverings_csv2json.sh
~~~

Now the coverings file should be created or updated:

- `data/basics/coverings/coverings_<number>.json`
