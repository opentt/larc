# Introduction

![Logo – red and yellow card](images/logo.png){ align=right }

!!! tip ""
	LARC – List of Authorized Racket Coverings

LARC is the "list of authorized racket coverings" for table tennis.
Here, you find an app and a REST-API to access this list.

This is **not** an official app or API of ITTF or DTTB or such.

The official list resides here:

- https://equipments.ittf.com/#/equipments/racket_coverings

As for this repository:

- feature requests, problems etc.: <https://gitlab.com/open-tt/larc/-/issues>
