# Legal stuff

![Logo – red and yellow card](images/logo.png){ align=right }


## License

License of the app: GNU General Public License.
See file [COPYING](https://gitlab.com/open-tt/larc/-/blob/main/COPYING).

Which means the app is free and open source

- you can use the program as you want, even commercially
- you can share the program as you like
- if you change the source code, you have to distribute it under the same license, and you have to provide the source code of the programs


## Copyright

Copyright 2019-2021 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See [COPYING](https://gitlab.com/open-tt/larc/-/blob/main/COPYING) for details.

This file is part of Open-TT: LARC.

Open-TT: LARC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Open-TT: LARC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Open-TT: LARC. If not, see <http://www.gnu.org/licenses/>.
